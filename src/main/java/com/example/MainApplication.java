package com.example;

import com.example.big.entity.BigEntityRepository;
import com.example.big.entity.SimpleBigEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
//@EnableCaching // Step 1: Дозволяємо використання кешування в контексті додатку
public class MainApplication {
    private final static Logger log = LoggerFactory.getLogger(MainApplication.class);

    @Configuration // Конфігуруємо запуск раннера при старті додатку
    static class Runner implements CommandLineRunner {

        private BigEntityRepository bigEntityRepository;

        Runner(BigEntityRepository bigEntityRepository) {
            this.bigEntityRepository = bigEntityRepository;
        }

        @Override // Виконання раннера
        public void run(String... args) {
            log.info(".... Fetching books");
            log.info("isbn-1234 -->" + bigEntityRepository.getByHash("isbn-1234"));
            log.info("isbn-4567 -->" + bigEntityRepository.getByHash("isbn-4567"));
            log.info("isbn-1234 -->" + bigEntityRepository.getByHash("isbn-1234"));
            log.info("isbn-4567 -->" + bigEntityRepository.getByHash("isbn-4567"));
            log.info("isbn-1234 -->" + bigEntityRepository.getByHash("isbn-1234"));
            log.info("isbn-4567 -->" + bigEntityRepository.getByHash("isbn-4567"));
        }
    }

    // Налаштування біна для репозиторія
    @Bean
    public SimpleBigEntityRepository simpleBigEntityRepository() {
        return new SimpleBigEntityRepository();
    }

    // Старт додатку
    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }
}
