package com.example.big.entity;

import org.springframework.cache.annotation.Cacheable;

// Фейковий репозиторій
public class SimpleBigEntityRepository implements BigEntityRepository {

    @Override
//    @Cacheable("BigEntity cache") // Step 2 Вказуємо метод, який буде кешуватись
    public BigEntity getByHash(String isbn) {

        // Виклик імітації роботи сервісу
        simulateSlowService();
        return new BigEntity("12", "Some title");
    }

    // Імітація роботи сервісу
    private void simulateSlowService() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
