package com.example.big.entity;

public class BigEntity {
    private String hash;
    private String title;

    public BigEntity(String hash, String title) {
        this.hash = hash;
        this.title = title;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "BigEntity{" +
                "hash='" + hash + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
