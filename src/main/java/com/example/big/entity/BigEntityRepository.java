package com.example.big.entity;

// Інтерфейс фейкового репозиторія
public interface BigEntityRepository {
    BigEntity getByHash(String isbn);
}
